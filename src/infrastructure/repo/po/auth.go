package po

type Auth struct {
	ID           int64
	Uid          int64
	FunctionId   int64
	BusinessId   int64
	AuthorizerId int64
}

type AuthResult struct {
	ID           int64
	Uid          int64
	FunctionId   int64
	FunctionName string
	BusinessId   int64
	BusinessName string
	AuthorizerId int64
}
