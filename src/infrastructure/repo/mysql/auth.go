package mysql

import (
	"gitlab.com/bishe-projects/common_utils/error_enum"
	"gitlab.com/bishe-projects/common_utils/mysql"
	"gitlab.com/bishe-projects/function_data_auth_service/src/infrastructure/repo/po"
)

var AuthMySQLDao = new(Auth)

type Auth struct{}

const FunctionDataAuthTable = "function_data_auth"

func (r *Auth) CreateAuth(auth *po.Auth) error {
	err := db.Table(FunctionDataAuthTable).Create(&auth).Error
	if err != nil {
		if code := mysql.MySqlErrCode(err); code == mysql.ErrDuplicateEntryCode {
			return error_enum.ErrDuplicateEntry
		}
		return err
	}
	return nil
}

func (r *Auth) GetAuthList(functionId int64, uid, businessId *int64) ([]*po.AuthResult, error) {
	var authList []*po.AuthResult
	d := db
	d = d.
		Table(FunctionDataAuthTable).
		Select("function_data_auth.id, function_data_auth.uid, function_data_auth.function_id, function.name as function_name, function_data_auth.business_id, business.name as business_name, function_data_auth.authorizer_id").
		Joins("join `function` on function_data_auth.function_id = function.id").
		Joins("join business on function_data_auth.business_id = business.id").
		Where("function_id = ?", functionId)
	if uid != nil {
		d = d.Where("uid = ?", *uid)
	}
	if businessId != nil {
		d = d.Where("business_id = ?", *businessId)
	}
	result := d.Scan(&authList)
	return authList, result.Error
}

func (r *Auth) GetAuth(functionId, uid, businessId int64) (*po.Auth, error) {
	var auth *po.Auth
	result := db.Table(FunctionDataAuthTable).Where("function_id = ? AND uid = ? AND business_id = ?", functionId, uid, businessId).First(&auth)
	return auth, result.Error
}
