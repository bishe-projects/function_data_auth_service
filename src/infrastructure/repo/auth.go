package repo

import (
	"gitlab.com/bishe-projects/function_data_auth_service/src/infrastructure/repo/mysql"
	"gitlab.com/bishe-projects/function_data_auth_service/src/infrastructure/repo/po"
)

type AuthDao interface {
	CreateAuth(auth *po.Auth) error
	GetAuthList(int64, *int64, *int64) ([]*po.AuthResult, error)
	GetAuth(int64, int64, int64) (*po.Auth, error)
}

var AuthMySQLRepo = NewAuthRepo(mysql.AuthMySQLDao)

type AuthRepo struct {
	AuthDao AuthDao
}

func NewAuthRepo(authDao AuthDao) *AuthRepo {
	return &AuthRepo{
		AuthDao: authDao,
	}
}
