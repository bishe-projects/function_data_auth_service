package biz_error

import "gitlab.com/bishe-projects/common_utils/business_error"

var (
	CreateAuthErr  = business_error.NewBusinessError("create function data auth failed", -10001)
	GetAuthErr     = business_error.NewBusinessError("get function data auth failed", -10002)
	GetAuthListErr = business_error.NewBusinessError("get function data auth list failed", -10003)
	AuthExistsErr  = business_error.NewBusinessError("the auth exists", -10004)
)
