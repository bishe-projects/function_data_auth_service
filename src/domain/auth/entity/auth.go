package entity

import (
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/common_utils/error_enum"
	"gitlab.com/bishe-projects/function_data_auth_service/src/infrastructure/biz_error"
	"gitlab.com/bishe-projects/function_data_auth_service/src/infrastructure/repo"
	"gitlab.com/bishe-projects/function_data_auth_service/src/infrastructure/repo/po"
	"gorm.io/gorm"
)

type Auth struct {
	ID           int64
	Uid          int64
	FunctionId   int64
	FunctionName string
	BusinessId   int64
	BusinessName string
	AuthorizerId int64
}

func (a *Auth) CreateAuth() *business_error.BusinessError {
	err := repo.AuthMySQLRepo.AuthDao.CreateAuth(a.ConvertToAuthPO())
	if err == error_enum.ErrDuplicateEntry {
		return business_error.DataExistsErr
	}
	if err != nil {
		klog.Errorf("[AuthAggregate] create auth failed: err=%s", err)
		return biz_error.CreateAuthErr
	}
	return nil
}

func (a *Auth) GetAuth(functionId, uid, businessId int64) *business_error.BusinessError {
	authPO, err := repo.AuthMySQLRepo.AuthDao.GetAuth(functionId, uid, businessId)
	if err == gorm.ErrRecordNotFound {
		return business_error.DataNotFoundErr
	}
	if err != nil {
		klog.Errorf("[AuthAggregate] get auth failed: err=%s", err)
		return biz_error.GetAuthErr
	}
	a.fillAuthFromAuthPO(authPO)
	return nil
}

type AuthList []*Auth

func (al *AuthList) GetAuthList(functionId int64, uid, businessId *int64) *business_error.BusinessError {
	authResultPOList, err := repo.AuthMySQLRepo.AuthDao.GetAuthList(functionId, uid, businessId)
	if err != nil {
		klog.Errorf("[AuthAggregate] get auth list failed: err=%s", err)
		return biz_error.GetAuthListErr
	}
	al.fillAuthListFromAuthResultPOList(authResultPOList)
	return nil
}

// converter

func (a *Auth) ConvertToAuthPO() *po.Auth {
	return &po.Auth{
		ID:           a.ID,
		Uid:          a.Uid,
		FunctionId:   a.FunctionId,
		BusinessId:   a.BusinessId,
		AuthorizerId: a.AuthorizerId,
	}
}

func (a *Auth) fillAuthFromAuthPO(authPO *po.Auth) {
	a.ID = authPO.ID
	a.Uid = authPO.Uid
	a.FunctionId = authPO.FunctionId
	a.BusinessId = authPO.BusinessId
	a.AuthorizerId = authPO.AuthorizerId
}

func (a *Auth) fillAuthFromAuthResultPO(authResultPO *po.AuthResult) {
	a.ID = authResultPO.ID
	a.Uid = authResultPO.Uid
	a.FunctionId = authResultPO.FunctionId
	a.FunctionName = authResultPO.FunctionName
	a.BusinessId = authResultPO.BusinessId
	a.BusinessName = authResultPO.BusinessName
	a.AuthorizerId = authResultPO.AuthorizerId
}

func (al *AuthList) fillAuthListFromAuthResultPOList(authPOList []*po.AuthResult) {
	*al = make([]*Auth, 0, len(authPOList))
	for _, authResultPO := range authPOList {
		auth := new(Auth)
		auth.fillAuthFromAuthResultPO(authResultPO)
		*al = append(*al, auth)
	}
}
