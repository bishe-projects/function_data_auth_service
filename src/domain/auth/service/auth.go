package service

import (
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/function_data_auth_service/src/domain/auth/entity"
)

var AuthDomain = new(Auth)

type Auth struct{}

func (d *Auth) CreateAuth(auth *entity.Auth) *business_error.BusinessError {
	return auth.CreateAuth()
}

func (d *Auth) GetAuthList(functionId int64, uid, businessId *int64) ([]*entity.Auth, *business_error.BusinessError) {
	authList := new(entity.AuthList)
	err := authList.GetAuthList(functionId, uid, businessId)
	return *authList, err
}

func (d *Auth) GetAuth(functionId, uid, businessId int64) (*entity.Auth, *business_error.BusinessError) {
	auth := new(entity.Auth)
	err := auth.GetAuth(functionId, uid, businessId)
	if err == business_error.DataNotFoundErr {
		return nil, nil
	}
	return auth, err
}
