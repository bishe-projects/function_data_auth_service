package facade

import (
	"context"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/common_utils"
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/function_data_auth_service/src/application/service"
	"gitlab.com/bishe-projects/function_data_auth_service/src/interface/assembler"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/function_data_auth"
)

var AuthFacade = new(Auth)

type Auth struct{}

func (f *Auth) CreateAuth(ctx context.Context, req *function_data_auth.CreateAuthReq) *function_data_auth.CreateAuthResp {
	resp := &function_data_auth.CreateAuthResp{}
	uid, err := common_utils.GetUidFromCtx(ctx)
	if err != nil {
		klog.CtxErrorf(ctx, "[AuthFacade] create auth failed: req=%+v, err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	err = service.AuthApp.CreateAuth(assembler.ConvertCreateAuthReqToAuthEntity(req, uid))
	if err != nil {
		klog.CtxErrorf(ctx, "[AuthFacade] create auth failed: req=%+v, err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
	}
	return resp
}

func (f *Auth) GetAuthList(ctx context.Context, req *function_data_auth.GetAuthListReq) *function_data_auth.GetAuthListResp {
	resp := &function_data_auth.GetAuthListResp{}
	authEntityList, err := service.AuthApp.GetAuthList(req.FunctionId, req.Uid, req.BusinessId)
	if err != nil {
		klog.CtxErrorf(ctx, "[AuthFacade] get auth list failed: req=%+v, err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	resp.AuthList = assembler.ConvertAuthEntityListToAuthList(authEntityList)
	return resp
}

func (f *Auth) HasAuth(ctx context.Context, req *function_data_auth.HasAuthReq) *function_data_auth.HasAuthResp {
	resp := &function_data_auth.HasAuthResp{}
	has, err := service.AuthApp.HasAuth(req.FunctionId, req.Uid, req.BusinessId)
	if err != nil {
		klog.CtxErrorf(ctx, "[AuthFacade] has auth failed: req=%+v, err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	resp.Has = has
	return resp
}
