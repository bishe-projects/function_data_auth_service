package assembler

import (
	"gitlab.com/bishe-projects/function_data_auth_service/src/domain/auth/entity"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/function_data_auth"
)

func ConvertCreateAuthReqToAuthEntity(req *function_data_auth.CreateAuthReq, uid int64) *entity.Auth {
	return &entity.Auth{
		Uid:          req.Uid,
		FunctionId:   req.FunctionId,
		BusinessId:   req.BusinessId,
		AuthorizerId: uid,
	}
}

func ConvertAuthEntityToAuth(authEntity *entity.Auth) *function_data_auth.FunctionDataAuth {
	return &function_data_auth.FunctionDataAuth{
		Id:           authEntity.ID,
		Uid:          authEntity.Uid,
		FunctionId:   authEntity.FunctionId,
		FunctionName: authEntity.FunctionName,
		BusinessId:   authEntity.BusinessId,
		BusinessName: authEntity.BusinessName,
		AuthorizerId: authEntity.AuthorizerId,
	}
}

func ConvertAuthEntityListToAuthList(authEntityList []*entity.Auth) []*function_data_auth.FunctionDataAuth {
	authList := make([]*function_data_auth.FunctionDataAuth, 0, len(authEntityList))
	for _, authEntity := range authEntityList {
		authList = append(authList, ConvertAuthEntityToAuth(authEntity))
	}
	return authList
}
