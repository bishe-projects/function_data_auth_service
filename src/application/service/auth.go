package service

import (
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/function_data_auth_service/src/domain/auth/entity"
	"gitlab.com/bishe-projects/function_data_auth_service/src/domain/auth/service"
	"gitlab.com/bishe-projects/function_data_auth_service/src/infrastructure/biz_error"
)

var AuthApp = new(Auth)

type Auth struct{}

func (a *Auth) CreateAuth(auth *entity.Auth) *business_error.BusinessError {
	err := service.AuthDomain.CreateAuth(auth)
	if err == business_error.DataExistsErr {
		return biz_error.AuthExistsErr
	}
	return err
}

func (a *Auth) GetAuthList(functionId int64, uid, businessId *int64) ([]*entity.Auth, *business_error.BusinessError) {
	return service.AuthDomain.GetAuthList(functionId, uid, businessId)
}

func (a *Auth) HasAuth(functionId, uid, businessId int64) (bool, *business_error.BusinessError) {
	auth, err := service.AuthDomain.GetAuth(functionId, uid, businessId)
	if err != nil {
		return false, err
	}
	return auth != nil, nil
}
