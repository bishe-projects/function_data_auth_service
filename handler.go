package main

import (
	"context"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/function_data_auth_service/src/interface/facade"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/function_data_auth"
)

// FunctionDataAuthServiceImpl implements the last service interface defined in the IDL.
type FunctionDataAuthServiceImpl struct{}

// CreateAuth implements the FunctionDataAuthServiceImpl interface.
func (s *FunctionDataAuthServiceImpl) CreateAuth(ctx context.Context, req *function_data_auth.CreateAuthReq) (resp *function_data_auth.CreateAuthResp, err error) {
	klog.CtxInfof(ctx, "CreateAuth req=%+v", req)
	resp = facade.AuthFacade.CreateAuth(ctx, req)
	klog.CtxInfof(ctx, "CreateAuth resp=%+v", resp)
	return
}

// GetAuthList implements the FunctionDataAuthServiceImpl interface.
func (s *FunctionDataAuthServiceImpl) GetAuthList(ctx context.Context, req *function_data_auth.GetAuthListReq) (resp *function_data_auth.GetAuthListResp, err error) {
	klog.CtxInfof(ctx, "GetAuthList req=%+v", req)
	resp = facade.AuthFacade.GetAuthList(ctx, req)
	klog.CtxInfof(ctx, "GetAuthList resp=%+v", resp)
	return
}

// HasAuth implements the FunctionDataAuthServiceImpl interface.
func (s *FunctionDataAuthServiceImpl) HasAuth(ctx context.Context, req *function_data_auth.HasAuthReq) (resp *function_data_auth.HasAuthResp, err error) {
	klog.CtxInfof(ctx, "HasAuth req=%+v", req)
	resp = facade.AuthFacade.HasAuth(ctx, req)
	klog.CtxInfof(ctx, "HasAuth resp=%+v", resp)
	return
}
