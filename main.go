package main

import (
	"github.com/cloudwego/kitex/server"
	function_data_auth "gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/function_data_auth/functiondataauthservice"
	"log"
	"net"
)

func main() {
	addr, _ := net.ResolveTCPAddr("tcp", "127.0.0.1:8884")
	svr := function_data_auth.NewServer(new(FunctionDataAuthServiceImpl), server.WithServiceAddr(addr))

	err := svr.Run()

	if err != nil {
		log.Println(err.Error())
	}
}
